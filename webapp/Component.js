jQuery.sap.declare("publicservices.her.myrequests.MYREQUESTExtenJunqiao.Component");
// use the load function for getting the optimized preload file if present

sap.ui.component.load({
	name: "publicservices.her.myrequests",
	// Use the below URL to run the extended application when SAP-delivered application is deployed on SAPUI5 ABAP Repository
	url: "/sap/bc/ui5_ui5/sap/MYREQUEST"
		// we use a URL relative to our own component
		// extension application is deployed with customer namespace
});

this.publicservices.her.myrequests.Component.extend("publicservices.her.myrequests.MYREQUESTExtenJunqiao.Component", {
	metadata: {
		manifest: "json"
	}


});