sap.ui.define([
	"publicservices/her/myrequests/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Sorter",
	"sap/m/GroupHeaderListItem",
	"sap/ui/Device",
	"publicservices/her/myrequests/model/formatter",
	"publicservices/her/myrequests/model/grouper",
	"publicservices/her/myrequests/util/Utils"
], function(B, J, F, a, S, G, D, f, g, U) {
	"use strict";
	return sap.ui.controller("publicservices.her.myrequests.MYREQUESTExtenJunqiao.controller.MasterCustom", {
			
			goToAPPPress : function(evt) {
			location.href = "http://de-pa-sap06.neovias.de:8000/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html?sap-client=100&sap-language=EN#Student-edit?TileType='PersonalDetails'";
		}

		

		//    formatter: f,
		//    onInit: function () {
		//        this._oComponent = this.getOwnerComponent();
		//        this._oRouter = this._oComponent.getRouter();
		//        this.setModel(new J(), "groupHeaderModel");
		//        var A = jQuery.sap.getUriParameters().get("appId") || "01", t = this, i = new sap.m.ObjectListItem({
		//                type: "{= ${device>/system/phone} ? 'Active' : 'Inactive'}",
		//                press: [
		//                    this.onSelect,
		//                    this
		//                ],
		//                title: "{Field06}",
		//                firstStatus: new sap.m.ObjectStatus({
		//                    text: "{Field04}",
		//                    state: {
		//                        path: "Status",
		//                        formatter: f.getStatus
		//                    }
		//                }).addStyleClass("objectStatusContainer"),
		//                attributes: [
		//                    new sap.m.ObjectAttribute({ text: "{Field05}" }),
		//                    new sap.m.ObjectAttribute({ text: "{i18n>createdOnLbl}:  {Field03}" })
		//                ]
		//            }), o = new sap.ui.model.Filter("AppId", sap.ui.model.FilterOperator.EQ, A), l = new sap.m.List("list", {
		//                busyIndicatorDelay: "{masterView>/delay}",
		//                noDataText: "{masterView>/noDataText}",
		//                mode: "{= ${device>/system/phone} ? 'None' : 'SingleSelectMaster'}",
		//                growing: true,
		//                growingThreshold: 1000,
		//                updateFinished: [
		//                    this.onUpdateFinished,
		//                    this
		//                ],
		//                select: [
		//                    this.onSelect,
		//                    this
		//                ],
		//                items: {
		//                    path: "/RequestSet",
		//                    template: i,
		//                    sorter: {
		//                        path: "Field06",
		//                        descending: false,
		//                        group: true
		//                    },
		//                    groupHeaderFactory: function (b) {
		//                        if (b) {
		//                            var c = b.text ? b.text : b.key;
		//                            if (t.getModel("groupHeaderModel").getProperty("/" + c)) {
		//                                c = t.getModel("groupHeaderModel").getProperty("/" + c);
		//                            } else {
		//                                t.updateGrpHeaderModel();
		//                                c = t.getModel("groupHeaderModel").getProperty("/" + c) ? t.getModel("groupHeaderModel").getProperty("/" + c) : c;
		//                            }
		//                            return new G({
		//                                title: c,
		//                                upperCase: false
		//                            });
		//                        }
		//                    },
		//                    filters: [o]
		//                },
		//                infoToolbar: new sap.m.Toolbar("filterBar", {
		//                    active: true,
		//                    visible: "{masterView>/isFilterBarVisible}",
		//                    press: [
		//                        this.onOpenViewSettings,
		//                        this
		//                    ],
		//                    title: new sap.m.Title("filterBarLabel", { text: "{masterView>/filterBarLabel}" })
		//                })
		//            });
		//        this.getView().byId("page").addContent(l);
		//        var v = new J({
		//                isFilterBarVisible: false,
		//                filterBarLabel: "",
		//                delay: 0,
		//                title: this.getResourceBundle().getText("masterTitle"),
		//                noDataText: this.getResourceBundle().getText("masterListNoDataText")
		//            }), O = l.getBusyIndicatorDelay();
		//        this._oList = l;
		//        this._oListFilterState = {
		//            aFilter: [],
		//            aSearch: []
		//        };
		//        this._oListSorterState = {
		//            aGroup: [],
		//            aSort: []
		//        };
		//        this.setModel(v, "masterView");
		//        l.attachEventOnce("updateFinished", function () {
		//            v.setProperty("/delay", O);
		//        });
		//        this.getView().addEventDelegate({
		//            onBeforeFirstShow: function () {
		//                this.getOwnerComponent().oListSelector.setBoundMasterList(l);
		//            }.bind(this)
		//        });
		//        this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
		//        this.getRouter().attachBypassed(this.onBypassed, this);
		//        this.byId("groupSelect").addDelegate({
		//            onAfterRendering: function (b) {
		//                var c = b.srcControl;
		//                $("#" + c.getId()).find(".sapUiIcon").attr("title", c.getTooltip());
		//            }
		//        });
		//    },
		//    updateGrpHeaderModel: function () {
		//        var o = this.getModel("groupHeaderModel");
		//        $.each(this._oList.getModel().oData, function (i, b) {
		//            if (!o.getProperty("/" + b.ContextId)) {
		//                o.setProperty("/" + b.ContextId, b.ContextDesc);
		//            }
		//            if (!o.getProperty("/" + b.Status)) {
		//                o.setProperty("/" + b.Status, b.StatusDesc);
		//            }
		//        });
		//    },
		//    onUpdateFinished: function (e) {
		//        this._updateListItemCount(e.getParameter("total"));
		//        this.byId("pullToRefresh").hide();
		//        this.updateGrpHeaderModel();
		//        if (!e.getParameter("total")) {
		//            this.getRouter().getTargets().display("detailObjectNotFound");
		//            this.isObjectNotFoundDisplayed = true;
		//            return;
		//        }
		//        if (this.isObjectNotFoundDisplayed) {
		//            this.getRouter().getTargets().display("object");
		//            this.isObjectNotFoundDisplayed = false;
		//        }
		//        if (this._oComponent.navToMasterView) {
		//            window.location.hash = "";
		//            var o = !this._oComponent.groupingEnabled ? e.getSource().getItems()[0] : e.getSource().getItems()[1], l = o.getModel().getProperty(o.getBindingContext().sPath);
		//            this.getRouter().navTo("object", {
		//                AppId: l.AppId,
		//                ContextId: l.ContextId,
		//                DecisionId: l.DecisionId,
		//                FormSubmId: l.FormSubmId,
		//                FormId: l.FormId,
		//                FormVersionNum: l.FormVersionNum,
		//                CreateDate: l.Field03
		//            }, true);
		//            this._oComponent.navToMasterView = false;
		//        }
		//    },
		//    onSearch: function (e) {
		//        this._oComponent.navToMasterView = true;
		//        if (e.getParameters().refreshButtonPressed) {
		//            this.onRefresh();
		//            return;
		//        }
		//        var q = e.getParameter("query"), A = jQuery.sap.getUriParameters().get("appId") || "01";
		//        if (q && q.length > 0) {
		//            this._oListFilterState.aSearch = [
		//                new sap.ui.model.Filter("AppId", sap.ui.model.FilterOperator.EQ, A),
		//                new F("SearchTerm", a.Contains, q)
		//            ];
		//        } else {
		//            this._oListFilterState.aSearch = [new sap.ui.model.Filter("AppId", sap.ui.model.FilterOperator.EQ, A)];
		//        }
		//        this._applyFilterSearch();
		//    },
		//    onRefresh: function () {
		//        this.getModel().resetChanges();
		//        this._oList.getBinding("items").refresh();
		//        this.getModel().refresh();
		//    },
		//    onSort: function (e) {
		//        var p = e.getSource().getSelectedItem().getKey();
		//        this._oListSorterState.aSort = new S(p, false);
		//        this._applyGroupSort();
		//    },
		//    onGroup: function (e) {
		//        var k = e.getSource().getSelectedItem().getKey(), o = { Group1: "" };
		//        if (k !== "None") {
		//            this._oListSorterState.aGroup = [new S(o[k], false, g[k].bind(e.getSource()))];
		//        } else {
		//            this._oListSorterState.aGroup = [];
		//        }
		//        this._applyGroupSort();
		//    },
		//    onOpenViewSettings: function () {
		//        if (!this.oViewSettingsDialog) {
		//            this.oViewSettingsDialog = sap.ui.xmlfragment("publicservices.her.myrequests.view.ViewSettingsDialog", this);
		//            this.getView().addDependent(this.oViewSettingsDialog);
		//            this.oViewSettingsDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
		//        }
		//        this.oViewSettingsDialog.open();
		//    },
		//    onSelect: function (e) {
		//        this._showDetail(e.getParameter("listItem") || e.getSource());
		//    },
		//    onBypassed: function () {
		//        this._oList.removeSelections(true);
		//    },
		//    createGroupHeader: function (o) {
		//        return new sap.m.GroupHeaderListItem({
		//            title: o.title,
		//            upperCase: false
		//        });
		//    },
		//    onNavBack: function () {
		//        var h = sap.ui.core.routing.History.getInstance(), p = h.getPreviousHash(), c = sap.ushell.Container.getService("CrossApplicationNavigation");
		//        if (p !== undefined) {
		//            history.go(-1);
		//        } else {
		//            c.toExternal({ target: { shellHash: "#" } });
		//        }
		//    },
		//    _onMasterMatched: function () {
		//        this.getOwnerComponent().oListSelector.oWhenListLoadingIsDone.then(function (p) {
		//            if (p.list.getMode() === "None") {
		//                return;
		//            }
		//            var A = p.firstListitem.getBindingContext().getProperty("AppId"), C = p.firstListitem.getBindingContext().getProperty("ContextId"), b = p.firstListitem.getBindingContext().getProperty("DecisionId"), c = p.firstListitem.getBindingContext().getProperty("FormSubmId"), d = p.firstListitem.getBindingContext().getProperty("FormId"), v = p.firstListitem.getBindingContext().getProperty("FormVersionNum"), e = p.firstListitem.getBindingContext().getProperty("Field03");
		//            this.getRouter().navTo("object", {
		//                AppId: A,
		//                ContextId: C,
		//                DecisionId: b,
		//                FormSubmId: c,
		//                FormId: d,
		//                FormVersionNum: v,
		//                CreateDate: e
		//            }, true);
		//        }.bind(this), function (p) {
		//            if (p.error) {
		//                return;
		//            }
		//            this.getRouter().getTargets().display("detailNoObjectsAvailable");
		//        }.bind(this));
		//    },
		//    _showDetail: function (i) {
		//        var r = !D.system.phone;
		//        this.getRouter().navTo("object", {
		//            AppId: i.getBindingContext().getProperty("AppId"),
		//            ContextId: i.getBindingContext().getProperty("ContextId"),
		//            DecisionId: i.getBindingContext().getProperty("DecisionId"),
		//            FormSubmId: i.getBindingContext().getProperty("FormSubmId"),
		//            FormId: i.getBindingContext().getProperty("FormId"),
		//            FormVersionNum: i.getBindingContext().getProperty("FormVersionNum"),
		//            CreateDate: i.getBindingContext().getProperty("Field03")
		//        }, r);
		//    },
		//    _updateListItemCount: function (t) {
		//        var T;
		//        if (this._oList.getBinding("items").isLengthFinal() && t) {
		//            T = this.getModel("REQUEST_CONFIG").getData()[0].AppDesc + " (" + t + ")";
		//        } else {
		//            T = this.getModel("REQUEST_CONFIG").getData()[0].AppDesc;
		//        }
		//        this.getModel("masterView").setProperty("/title", T);
		//    },
		//    _applyFilterSearch: function () {
		//        var b = this._oListFilterState.aSearch.concat(this._oListFilterState.aFilter), v = this.getModel("masterView");
		//        this._oList.getBinding("items").filter(b, "Application");
		//        if (b.length !== 0) {
		//            v.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataWithFilterOrSearchText"));
		//        } else if (this._oListFilterState.aSearch.length > 0) {
		//            v.setProperty("/noDataText", this.getResourceBundle().getText("masterListNoDataText"));
		//        }
		//    },
		//    _applyGroupSort: function () {
		//        var s = this._oListSorterState.aGroup.concat(this._oListSorterState.aSort);
		//        this._oList.getBinding("items").sort(s);
		//    },
		//    _updateFilterBar: function (s) {
		//        var v = this.getModel("masterView");
		//        v.setProperty("/isFilterBarVisible", this._oListFilterState.aFilter.length > 0);
		//        v.setProperty("/filterBarLabel", this.getResourceBundle().getText("masterFilterBarText", [s]));
		//    },
		//    onCreatePress: function (e) {
		//        this._oRouter.navTo("createRequest", {}, true);
		//    },
		//    handleGroup: function (e) {
		//        var s = [], i = e.getParameter("selectedItem"), k = i ? i.getKey() : null, o = new sap.ui.model.Sorter(k, true, true), l = sap.ui.getCore().byId("list"), b = l.getBinding("items");
		//        this._oComponent.groupingEnabled = true;
		//        if (k !== null && k !== "None") {
		//            s.push(o);
		//        } else {
		//            this._oComponent.groupingEnabled = false;
		//        }
		//        b.sort(s);
		//    }
	});
});