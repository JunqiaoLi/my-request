sap.ui.define([
	"publicservices/her/myrequests/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"publicservices/her/myrequests/model/formatter",
	"publicservices/her/myrequests/util/Utils",
	'sap/m/Dialog',
	'sap/m/MessageToast',
	'sap/m/Input',
	'sap/m/Button',
	'sap/m/DatePicker'
], function(B, J, f, U, Dialog, MessageToast, Input, Button, DatePicker) {
	"use strict";

	function G(p, v) {
		this.CONFIG_MAPPING = "";
		this.PARAM = p;
		this.PARAMVALUE = v;
	}
	return sap.ui.controller("publicservices.her.myrequests.MYREQUESTExtenJunqiao.controller.DetailCustom", {
		onDisplayDetailsButtonPress: function(oEvent) {
				
				var that = this;
				if (!that.pressDialog) {
					that.pressDialog = new Dialog({
						title: 'Detailed Information',
						content: [ new Input({
							value: '{Field06}',
							enabled: false
						}),new Input({
							value: '{Field05}'+'                                                                             '+'{Field04}',
							enabled: false
						}),new Input({
							value: '{Field01}',
							enabled: false
						}),new Input({
							value: '{Field03}',
							enabled: false
						}),new DatePicker({
							placeholder: '{Field03}'
						}),new DatePicker({
							valueFormat: "yyyy-MM-dd",
							displayFormat: "short",
							placeholder: '{Field03}'
						}),new DatePicker({
							valueFormat: "yyyy-MM-dd",
							displayFormat: "MMM, d, y",
							placeholder: '{Field03}'
						}),new DatePicker({
							valueFormat: "yyyy-MM-dd",
							displayFormat: "dd.MM.yyyy",
							placeholder: '{Field03}'
						}),new DatePicker({
							valueFormat: "yyyy-MM-dd",
							displayFormat: "yyyy-MM-dd",
							placeholder: '{Field03}'
						})],
						beginButton: new Button({
							text: 'Close',
							press: function() {
								that.pressDialog.close();
							}
						})
					});
					
					//to get access to the global model
					this.getView().addDependent(that.pressDialog);
				}
				
				that.pressDialog.open();
			
		},
		onDisplayDetailsButtonNum2Press: function(oEvent) {
				var that1 = this;
				if (!that1.pressDialog1) {
					that1.pressDialog1 = new Dialog({
						title: 'Detailed Information for Button Number 2',
						content: [ new Input({
							value: '{Field06}',
							enabled: false
						}),new Input({
							value: '{Field05}'+'                                                                             '+'{Field04}',
							enabled: false
						}),new Input({
							value: '{Field01}',
							enabled: false
						}),new Input({
							value: '{Field03}',
							enabled: false
						})],
						beginButton: new Button({
							text: 'Close',
							press: function() {
								that1.pressDialog1.close();
							}
						})
					});
					
					//to get access to the global model
					this.getView().addDependent(that1.pressDialog1);
				}
				
				that1.pressDialog1.open();
			
		}
		
		//    formatter: f,
		//    onInit: function () {
		//        this._oComponent = this.getOwnerComponent();
		//        this._oRouter = this._oComponent.getRouter();
		//        var v = new J({
		//            busy: false,
		//            delay: 0,
		//            lineItemListTitle: this.getResourceBundle().getText("detailLineItemTableHeading")
		//        });
		//        this.getRouter().getRoute("object").attachPatternMatched(this._onObjectMatched, this);
		//        this.setModel(v, "detailView");
		//        this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataLoaded.bind(this));
		//    },
		//    onShareEmailPress: function () {
		//        var v = this.getModel("detailView");
		//        sap.m.URLHelper.triggerEmail(null, v.getProperty("/shareSendEmailSubject"), v.getProperty("/shareSendEmailMessage"));
		//    },
		//    onShareInJamPress: function () {
		//        var v = this.getModel("detailView"), s = sap.ui.getCore().createComponent({
		//                name: "sap.collaboration.components.fiori.sharing.dialog",
		//                settings: {
		//                    object: {
		//                        id: location.href,
		//                        share: v.getProperty("/shareOnJamTitle")
		//                    }
		//                }
		//            });
		//        s.open();
		//    },
		//    onListUpdateFinished: function (e) {
		//        var t, T = e.getParameter("total"), v = this.getModel("detailView");
		//        if (this.byId("lineItemsList").getBinding("items").isLengthFinal()) {
		//            if (T) {
		//                t = this.getResourceBundle().getText("detailLineItemTableHeadingCount", [T]);
		//            } else {
		//                t = this.getResourceBundle().getText("detailLineItemTableHeading");
		//            }
		//            v.setProperty("/lineItemListTitle", t);
		//        }
		//    },
		//    _onObjectMatched: function (e) {
		//        var c = e.getParameter("arguments").ContextId, a = e.getParameter("arguments").AppId, F = e.getParameter("arguments").FormSubmId, C = e.getParameter("arguments").CreateDate, D = e.getParameter("arguments").DecisionId, b = e.getParameter("arguments").FormVersionNum, o = "/RequestSet(AppId='" + a + "',ContextId='" + c + "',DecisionId='" + e.getParameter("arguments").DecisionId + "',FormSubmId='" + F + "')";
		//        this.sFormId = e.getParameter("arguments").FormId;
		//        this._GFDFData.update = false;
		//        this._GFDFData.domainUpdate = false;
		//        this._oGeneralTab = null;
		//        this.sAppId = a;
		//        this.sContextId = c;
		//        this.FormSubmId = F;
		//        this.sDecisionId = D;
		//        this.CreateDate = C;
		//        this.FormVersionNum = b;
		//        if (a && c) {
		//            this._bindView(o);
		//            this.bindIconTabBar(c, a);
		//            this.getActionData();
		//        }
		//    },
		//    getActionData: function () {
		//        this.byId("DynamicActionBar").getController().getActionData(this.sContextId, this.FormSubmId, this.sAppId, this.sDecisionId, this);
		//    },
		//    bindIconTabBar: function (c, a) {
		//        var i = this.byId("idIconTabBar");
		//        i.unbindAggregation("items");
		//        var t = this;
		//        this._oComponent.reqConfigModelLoaded.done(function () {
		//            if (t.getModel(c + "_" + a) && t.getModel(c + "_" + a).getData().results.length > 0) {
		//                i.setVisible(true);
		//                i.bindAggregation("items", {
		//                    path: c + "_" + a + ">/results",
		//                    factory: $.proxy(t.iconsFactory, t)
		//                });
		//            } else {
		//                i.setVisible(false);
		//                i.removeAllContent();
		//                i.removeAllItems();
		//            }
		//        });
		//    },
		//    _bindView: function (o) {
		//        var v = this.getModel("detailView"), t = this;
		//        v.setProperty("/busy", false);
		//        this.getView().unbindElement();
		//        this.getView().bindElement({
		//            path: o,
		//            parameters: { expand: "GeneralSet,HoldSet,FeeSet,RequiredDocSet,GenDocSet,AssociatedStudyOfferSet" },
		//            events: {
		//                change: this._onBindingChange.bind(this),
		//                dataRequested: function () {
		//                    v.setProperty("/busy", true);
		//                },
		//                dataReceived: function () {
		//                    var m = this.getModel(), c = m.getContext(this.getPath()), d = m.getProperty("DocumentCount", c);
		//                    v.setProperty("/status", m.getProperty("Status", c));
		//                    if (t.iconTabFilter && parseInt(d) > 0 && v.getProperty("/status") === "IADH4") {
		//                        t.iconTabFilter.setCount(d);
		//                        t.iconTabFilter.setIconColor("Critical");
		//                    } else if (t.iconTabFilter) {
		//                        t.iconTabFilter.setCount("");
		//                        t.iconTabFilter.setIconColor("Default");
		//                    }
		//                    v.setProperty("/busy", false);
		//                }
		//            }
		//        });
		//    },
		//    _onBindingChange: function () {
		//        var v = this.getView(), e = v.getElementBinding();
		//        if (!e.getBoundContext()) {
		//            this.getRouter().getTargets().display("detailObjectNotFound");
		//            this.getOwnerComponent().oListSelector.clearMasterListSelection();
		//            return;
		//        }
		//        this._getFormData(this.sFormId, this.sContextId, this.FormSubmId, this.FormVersionNum);
		//        var p = e.getPath();
		//        this.getOwnerComponent().oListSelector.selectAListItem(p);
		//        var r = this.getResourceBundle(), o = v.getModel().getObject(p);
		//        this.sFormSubmissionId = this.FormSubmId;
		//        this.iVersion = o.FormVersionNum;
		//        if (o) {
		//            var O = o.AppId, s = o.Field01, V = this.getModel("detailView");
		//            this.sContextId = o.ContextId;
		//            this.sFormSubmissionId = o.FormSubmId;
		//            this.sDecisionId = o.DecisionId;
		//            V.setProperty("/detailTitle", o.Field07);
		//            V.setProperty("/status", o.Status);
		//            V.setProperty("/enableSubmitBtn", false);
		//            V.setProperty("/saveAsTileTitle", r.getText("shareSaveTileAppTitle", [s]));
		//            V.setProperty("/shareOnJamTitle", s);
		//            V.setProperty("/shareSendEmailSubject", r.getText("shareSendEmailObjectSubject", [O]));
		//            V.setProperty("/shareSendEmailMessage", r.getText("shareSendEmailObjectMessage", [
		//                s,
		//                O,
		//                location.href
		//            ]));
		//        }
		//    },
		//    _getFormData: function (F, c, n, v) {
		//        this._oComponent.getGFDModule().getFormData({
		//            oDataModel: this._oComponent.getConnectorInstance("gfdService"),
		//            sFormId: F,
		//            sObjectId: F,
		//            sContextId: c,
		//            sNotificationId: n,
		//            iVersion: v
		//        }, $.proxy(this._formDataCallback, this), $.proxy(this._domainDataCallback, this));
		//    },
		//    _oGeneralTab: null,
		//    _GFDFData: {
		//        "update": false,
		//        "config": null,
		//        "metadata": null,
		//        "info": null,
		//        "document": null,
		//        "domainUpdate": false,
		//        "domainMap": null
		//    },
		//    _formDataCallback: function (c, F, o, d) {
		//        this._GFDFData.update = true;
		//        this._GFDFData.config = c;
		//        this._GFDFData.metadata = F;
		//        this._GFDFData.info = o;
		//        this._GFDFData.document = d;
		//        if (this.deferred) {
		//            this.deferred.resolve();
		//        }
		//    },
		//    _bindGFDView: function (v) {
		//        if (v) {
		//            this._oGeneralTab = v;
		//            var l = this._oComponent.getModel("config").getProperty("/selectedLanguage");
		//            v.setModel(this._GFDFData.metadata).bindElement("/" + l);
		//            v.setModel(this._GFDFData.config, "FormConfig");
		//            v.setModel(this._GFDFData.info, "FormDetails");
		//            v.setModel(this._GFDFData.document, "DocumentQuestions");
		//        }
		//    },
		//    _domainDataCallback: function (d) {
		//        this._GFDFData.domainUpdate = true;
		//        this._GFDFData.domainMap = d;
		//        this._bindGFDViewDomainValues();
		//    },
		//    _bindGFDViewDomainValues: function () {
		//        if (this._oGeneralTab && this._GFDFData.domainUpdate) {
		//            var t = this, d = this._GFDFData.domainMap;
		//            $.each(d, function (i, D) {
		//                if (!t._oGeneralTab.getModel(i)) {
		//                    var j = new sap.ui.model.json.JSONModel();
		//                    j.setSizeLimit(D.length + 1);
		//                    j.setData({ domainValue: D });
		//                    t._oGeneralTab.setModel(j, i);
		//                }
		//            });
		//        }
		//    },
		//    _onMetadataLoaded: function () {
		//        var o = this.getView().getBusyIndicatorDelay(), v = this.getModel("detailView");
		//        v.setProperty("/delay", 0);
		//        v.setProperty("/lineItemTableDelay", 0);
		//        v.setProperty("/busy", true);
		//        v.setProperty("/delay", o);
		//    },
		//    iconsFactory: function (i, c) {
		//        var t = this;
		//        var a = new sap.m.IconTabFilter({
		//            key: c.getProperty("TabId"),
		//            text: c.getProperty("TabDescription")
		//        });
		//        a.setTooltip(c.getProperty("TabDescription"));
		//        var S = new Date(c.getProperty("StartDate")).setHours(0, 0, 0, 0), E = new Date(c.getProperty("EndDate")).setHours(0, 0, 0, 0), T = c.getProperty("TabVisibleInd"), C = new Date(this.CreateDate).setHours(0, 0, 0, 0);
		//        switch (c.getProperty("TabAssociationName")) {
		//        case "GeneralSet":
		//            var g = null;
		//            var d = null;
		//            a.setIcon("sap-icon://hint");
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.GeneralTab",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//            });
		//            if (t._GFDFData.update) {
		//            } else {
		//                t.deferred = $.Deferred();
		//                t.deferred.done(function () {
		//                    sap.ui.view({
		//                        type: sap.ui.core.mvc.ViewType.JS,
		//                        viewName: "publicservices.her.myrequests.view.formviews.DisplayLayout",
		//                        async: true
		//                    }).loaded().then(function (v) {
		//                        t._bindGFDView(v);
		//                        t._bindGFDViewDomainValues();
		//                        a.addContent(v);
		//                    });
		//                });
		//            }
		//            break;
		//        case "HoldSet":
		//            a.setIcon("sap-icon://warning");
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.Holds",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//            });
		//            break;
		//        case "FeeSet":
		//            a.setIcon("sap-icon://money-bills");
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.Fees",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//            });
		//            break;
		//        case "RequiredDocSet":
		//            a.setIcon("sap-icon://documents");
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.RequiredDocuments",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//                t.getDocumentErrorCount(a);
		//            });
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.GeneratedDocuments",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//            });
		//            break;
		//        case "GenDocSet":
		//            a.setIcon("sap-icon://documents");
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.RequiredDocuments",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//                t.getDocumentErrorCount(a);
		//            });
		//            sap.ui.view({
		//                type: sap.ui.core.mvc.ViewType.XML,
		//                viewName: "publicservices.her.myrequests.view.subviews.GeneratedDocuments",
		//                async: true
		//            }).loaded().then(function (v) {
		//                a.addContent(v);
		//            });
		//            break;
		//        }
		//        if (T === "X" && new Date(C) >= new Date(S) && new Date(C) <= new Date(E)) {
		//            a.setVisible(true);
		//        } else {
		//            a.setVisible(false);
		//            a.destroyContent();
		//            $(".sapMITBContainerContent").hide();
		//        }
		//        return a;
		//    },
		//    getDocumentErrorCount: function (i) {
		//        this.iconTabFilter = i;
		//        var I = this.byId("idIconTabBar"), v = this.getModel("detailView");
		//        v.setProperty("/invalidDoc", false);
		//        I.addDelegate({
		//            onAfterRendering: function () {
		//                var d = I.getBindingContext() ? I.getModel().getProperty(I.getBindingContext().sPath).DocumentCount : 0;
		//                if (d && v.getProperty("/status") === "IADH4") {
		//                    v.setProperty("/invalidDoc", true);
		//                    i.setCount(d);
		//                    i.setIconColor("Critical");
		//                }
		//            }
		//        });
		//    },
		//    withdrawRequest: function (e) {
		//        U.showBusy();
		//        var d = new sap.ui.model.odata.ODataModel("/sap/opu/odata/SAP/GFD_CONFIG_SRV", true), a = e.getSource().getModel(), t = this, p = "GFD_FormSubmissionCollection(ContextId='" + this.sContextId + "',ExtendedId='',FormSubmId='" + this.FormSubmId + "')";
		//        var b = jQuery.proxy(function (c, m) {
		//            U.hideBusy();
		//        }, this);
		//        d.update(p, {
		//            "ContextId": t.sContextId,
		//            "FormSubmId": t.FormSubmId,
		//            "FormId": t.sFormId,
		//            "ExtendedId": "",
		//            "Action": "IADHB"
		//        }, null, function (D, r) {
		//            U.hideBusy();
		//            sap.m.MessageToast.show(t.getResourceBundle().getText("withdrawConfirmMsg", [t.FormSubmId]));
		//            t.getModel("detailView").setProperty("/status", "IADHB");
		//            t.modelRefresh();
		//        }, b, this);
		//    },
		//    submitDocuments: function (e) {
		//        U.showBusy();
		//        var F = sap.ui.getCore().byId("docsUploadForm"), q = F.getFormContainers()[0].getFormElements(), m = F.getModel(), t = this, r = m.getProperty(F.getBindingContext().getPath()), d = [], a = 0;
		//        $.each(q, function (i, o) {
		//            var b = m.getProperty(o.getBindingContext().getPath());
		//            if (b.FileNumber && b.Status !== "02") {
		//                d.push({
		//                    "QUESTION": b.QuestionId,
		//                    "DOC_TYPE": b.FileType,
		//                    "DOC_ID": b.FileNumber,
		//                    "DOC_ID2": r.DecisionId,
		//                    "DOC_VERSION": b.DocVer,
		//                    "DOC_PART": "",
		//                    "TEXT": b.QuestionText,
		//                    "FILE_NAME": b.FileName
		//                });
		//            } else if (b.Status === "02") {
		//                a++;
		//            }
		//        });
		//        if (a) {
		//            U.hideBusy();
		//            U.showMessage(sap.ui.core.ValueState.Error, sap.ui.core.ValueState.Error, t.getResourceBundle().getText("documentsErrorMsg"));
		//            return;
		//        }
		//        var D = new sap.ui.model.odata.ODataModel("/sap/opu/odata/SAP/GFD_CONFIG_SRV", true);
		//        D.create("GFD_FormSubmissionCollection", {
		//            "ContextId": r.ContextId,
		//            "FormId": t.sFormId,
		//            "FormSubmId": r.FormSubmId,
		//            "Action": r.Status,
		//            "ExtendedId": "",
		//            "CustomContent": "",
		//            "Content": "",
		//            "DocContent": JSON.stringify(d)
		//        }, null, jQuery.proxy(function (o, R) {
		//            U.hideBusy();
		//            sap.m.MessageToast.show(t.getResourceBundle().getText("docSubConfirmMsg", [o.FormSubmId]));
		//            t.modelRefresh();
		//            t.getModel("detailView").setProperty("/status", "IADH2");
		//        }, this), jQuery.proxy(function (b, c) {
		//            U.hideBusy();
		//            U.showMessage(sap.ui.core.ValueState.Error, sap.ui.core.ValueState.Error, this.getResourceBundle().getText("fillAllManFieldsMsg"));
		//        }, this));
		//    },
		//    modelRefresh: function () {
		//        var l = sap.ui.getCore().byId("list");
		//        l.getBinding("items").refresh();
		//        this.getActionData();
		//    },
		//    onAcceptChangeProgram: function (e) {
		//        this._oComponent.getGFDModule().acceptDecision({
		//            "ContextId": this.sContextId,
		//            "FormSubmId": this.sFormSubmissionId,
		//            "FormId": this.sFormId,
		//            "ExtendedId": ""
		//        }, $.proxy(this._onSuccessfulAccept, this));
		//    },
		//    _onSuccessfulAccept: function (d, r) {
		//        var m = this.getResourceBundle().getText("formAccepted", [this.sFormSubmissionId]);
		//        setTimeout(function () {
		//            sap.m.MessageToast.show(m);
		//        }, 0);
		//        this.modelRefresh();
		//    },
		//    onRegisterChangeProgram: function (e) {
		//        var c = [];
		//        c.push(new G("DECISION_ID", this.sDecisionId));
		//        c.push(new G("FormSubmId", this.sFormSubmissionId));
		//        if (this.sDecisionId && this.sFormSubmissionId) {
		//            this._oComponent.getGFDModule().registerDecision({
		//                "ContextId": this.sContextId,
		//                "FormId": this.sFormId,
		//                "DECISION_ID": this.sDecisionId,
		//                "Content": c
		//            }, $.proxy(this._startRegistration, this));
		//        }
		//    },
		//    _startRegistration: function (d, r) {
		//        var D = d.RedirectUrl.split("#")[1].split("/");
		//        this._oRouter.navTo("register", {
		//            ContextId: D[2],
		//            FormId: D[6],
		//            FormSubmId: this.sFormSubmissionId || "",
		//            DecisionId: d.PrevDecisionid || this.sDecisionId || "",
		//            ProcedureId: d.ProcedureId || "",
		//            CourseOffering: d.CourseOffering || "",
		//            AppId: this.sAppId || "",
		//            ObjectId: d.ObjectIds || ""
		//        });
		//    },
		//    withdrawRequestConfirmMsg: function (e) {
		//        var t = this;
		//        t.tempEvent = $.extend(true, {}, e);
		//        var c = $.Deferred();
		//        c.done(function (u) {
		//            if (!u) {
		//                return;
		//            }
		//            t.withdrawRequest(t.tempEvent);
		//        });
		//        U.confirmMessage(this.getResourceBundle().getText("withdrawConfirmTitle"), this.getResourceBundle().getText("withdrawRequestConfirmMsg"), c);
		//    },
		//    submitDocumentsConfirmMsg: function (e) {
		//        var t = this;
		//        t.tempEvent = $.extend(true, {}, e);
		//        var c = $.Deferred();
		//        c.done(function (u) {
		//            if (!u) {
		//                return;
		//            }
		//            t.submitDocuments(t.tempEvent);
		//        });
		//        U.confirmMessage(this.getResourceBundle().getText("submitConfirmTitle"), this.getResourceBundle().getText("resubmitConfirmMsg"), c);
		//    },
		//    onAcceptChangeProgramConfirmMsg: function (e) {
		//        var t = this;
		//        t.tempEvent = $.extend(true, {}, e);
		//        var c = $.Deferred();
		//        c.done(function (u) {
		//            if (!u) {
		//                return;
		//            }
		//            t.onAcceptChangeProgram(t.tempEvent);
		//        });
		//        U.confirmMessage(this.getResourceBundle().getText("acceptCOPConfirmTitle"), this.getResourceBundle().getText("acceptCOPConfirmMsg"), c);
		//    },
		//    onRegisterChangeProgramMsg: function (e) {
		//        var t = this;
		//        t.tempEvent = $.extend(true, {}, e);
		//        var c = $.Deferred();
		//        c.done(function (u) {
		//            if (!u) {
		//                return;
		//            }
		//            t.onRegisterChangeProgram(t.tempEvent);
		//        });
		//        U.confirmMessage(this.getResourceBundle().getText("registerCOPConfirmTitle"), this.getResourceBundle().getText("registerCOPConfirmMsg"), c);
		//    }
	});
});