sap.ui.define([
	"publicservices/her/myrequests/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"publicservices/her/myrequests/model/formatter",
	"publicservices/her/myrequests/util/Utils",
	'sap/m/Input',
	'sap/m/Button',
	'sap/m/DatePicker',
	'sap/m/Dialog'
], function(B, J, f, U, Input, Button, DatePicker, Dialog) {
	"use strict";
	return sap.ui.controller("publicservices.her.myrequests.MYREQUESTExtenJunqiao.controller.ActionBarCustom", {
		formatter: f,
		onInit: function() {},
		getActionData: function(c, n, a, D, b) {
			this._oComponent = this.getOwnerComponent();
			this.DetailController = b;
			var r = {};
			var e = "?$expand=AllowedActionSet";
			r.method = "GET";
			r.url = "/RequestSet(AppId='" + a + "',ContextId='" + c + "',DecisionId='" + D + "',FormSubmId='" + n + "')" + e;
			this._oComponent.getConnectorInstance("mainService").processRequest(r, $.proxy(this._onReceivingActionCodes, this));
		},
		_onReceivingActionCodes: function(d, R) {
			var a = this.getView().getModel("actionCodes");
			var t = this;
			a.setData(d.AllowedActionSet);
			var aNewProperty = a.getProperty("/results");
			var oNewResult = {
				Action: "IADHBBB",
				ActionDesc: "DisplayNum2",
				ActionTooltip: "DisplayNum2",
				Allowed: true,
				ContextId: "PIQ_LOA",
				DecisionId: "0",
				Emphasize: "",
				FormSubmId: "",
				ReqAppId: "",
				SequenceNo: 10,
				Status: "IADH2"
			};
			aNewProperty.push(oNewResult);
			a.setProperty("/results", aNewProperty);
			t.getView().setModel(a);
		},
		allowedActions: function(i, c) {
			if (c.getPath() === "/results/0") {
				var a = this.getView().byId("dynamicActionBarHbox");
				a.insertContent(new sap.m.ToolbarSpacer());
			}
			var t = this;
			var b = new sap.m.Button({
				text: c.getProperty("ActionDesc"),
				customData: [new sap.ui.core.CustomData({
					key: "Action",
					value: c.getProperty("Action")
				})],
				type: f.emphasizeBtn(c.getProperty("Emphasize")),
				tooltip: c.getProperty("ActionTooltip"),
				visible: f.actionBarBtnVisiblie(c.getProperty("Allowed")),
				press: function(e) {
					var d = e.getSource().getCustomData()[0].getValue();
					var g = t.DetailController;
					switch (d) {
						case "IADHA":
							g.onAcceptChangeProgramConfirmMsg(e);
							break;
						case "IADH6":
							g.onRegisterChangeProgramMsg(e);
							break;
						case "IADH4":
							g.submitDocumentsConfirmMsg(e);
							break;
						case "IADHB":
							g.withdrawRequestConfirmMsg(e);
							break;
						case "IADHBBB":
							g.onDisplayDetailsButtonNum2Press(e);
							break;
						default:
							break;
					}
				}
			});
			return b;

		}

	});
});